#include "main.h"

#ifndef RING_H
#define RING_H


class Ring {
public:
    Ring() {}
    Ring(float x, float y, float radius, color_t color);
    glm::vec3 position;
    float rotation;
    void draw(glm::mat4 VP);
    void set_position(float x, float y);
    int tick(float flag);
    double speed;
    float radius;
    bounding_box_t bounding_box();
private:
    VAO *ring;
};

#endif // CIRCLE_H
