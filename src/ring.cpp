#include <cmath>
#include <vector>
#include <utility>
#include "ring.h"

using namespace std; 

Ring::Ring(float x, float y, float radius, color_t color) {
    this->position = glm::vec3(x, y, 0);
    this->rotation = 50;
    speed = 0.01;
    this->radius = 0.5;
    
    GLfloat vertex_buffer_data[10000];
    int k = 0;
    for (float i = 0.0; i <= 3.14; i += 0.001)
    {
        vertex_buffer_data[k++] = sin(i)*radius; 
        vertex_buffer_data[k++] = cos(i)*radius;
    }
    this->ring = create3DObject(GL_POINTS, 1600, vertex_buffer_data, color, GL_FILL);
}

void Ring::draw(glm::mat4 VP) {
    Matrices.model = glm::mat4(1.0f);
    glm::mat4 translate = glm::translate (this->position);    // glTranslatef
    glm::mat4 rotate    = glm::rotate((float) (this->rotation * M_PI / 180.0f), glm::vec3(0, 0, 1));
    // No need as coords centered at 0, 0, 0 of cube arouund which we waant to rotate
    // rotate          = rotate * glm::translate(glm::vec3(0, -0.6, 0));
    Matrices.model *= (translate * rotate);
    glm::mat4 MVP = VP * Matrices.model;
    glUniformMatrix4fv(Matrices.MatrixID, 1, GL_FALSE, &MVP[0][0]);
    draw3DObject(this->ring);
}

void Ring::set_position(float x, float y) {
    this->position = glm::vec3(x, y, 0);
}

int Ring::tick(float flag) {
    this->position.x -= flag;
    return this->position.x;
}

bounding_box_t Ring::bounding_box() {
    float x = this->position.x, y = this->position.y;
    bounding_box_t box;
    box.type = 0;
    box.x = x-radius; box.y = y;
    box.width = radius*2; box.height = radius;
    return box;
}
