#include <cmath>
#include <vector>
#include <utility>
#include "magnet.h"
#include "line.h"

using namespace std; 

Magnet::Magnet(float x, float y) {
    this->position = glm::vec3(x, y, 0);
    this->speed = 0.01;
    this->rotation = 0;
    width = 0.3; height = 0.5;
    lines.push_back(Line(x, y+height, x+width, y+height, 0.03f, COLOR_BROWN));
    lines.push_back(Line(x, y, x, y+height, 0.03f, COLOR_BROWN));
    lines.push_back(Line(x+width, y, x+width, y+height, 0.03f, COLOR_BROWN));
}

void Magnet::draw(glm::mat4 VP) {
    for (int i=0; i<lines.size(); i++)
    {
        lines[i].draw(VP);
    }
}

void Magnet::set_position(float x, float y) {
    this->position = glm::vec3(x, y, 0);
}

int Magnet::tick(float flag) {
    for (int i=0; i<lines.size(); i++)
    {
        lines[i].tick(flag, 0);
    }
    //this->position.x -= flag;
    //return this->position.x;
}

bounding_box_t Magnet::bounding_box() {
    float x = this->position.x, y = this->position.y;
    bounding_box_t box;
    box.x = x;
    box.y = 0;
    box.type = 0;
    box.width = width; box.height = height;
    return box;
}
