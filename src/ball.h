#include "main.h"

#ifndef BALL_H
#define BALL_H


class Ball {
public:
    Ball() {}
    Ball(float x, float y, int type);
    glm::vec3 position;
    float rotation;
    color_t color;
    int type;
    void draw(glm::mat4 VP);
    void set_position(float x, float y);
    int tick(float flag);
    double speed;
    bounding_box_t bounding_box();
private:
    VAO *object;
};

#endif // BALL_H
