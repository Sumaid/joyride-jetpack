#include "main.h"

#ifndef PLAYER_H
#define PLAYER_H


class Player {
public:
    Player() {}
    Player(float x, float y, color_t color, int flag);
    glm::vec3 position;
    float rotation;
    int flag;
    float xspeed, yspeed, up_press, down_press, acceleration;
    void draw(glm::mat4 VP);
    void set_position(float x, float y);
    void tick();
    void shield_change(int flag);
    //void pull();
    void move(int left, int right, int up);
    double speed;
    bounding_box_t bounding_box();
private:
    VAO *object, *shield;
};

#endif // PLAYER_H
