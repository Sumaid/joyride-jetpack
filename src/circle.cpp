#include <cmath>
#include <vector>
#include <utility>
#include "circle.h"

using namespace std; 
vector< pair<double, double> > points;

Circle::Circle(float x, float y, float length, color_t color) {
    this->position = glm::vec3(x, y, 0);
    this->rotation = 0;
    speed = 0.01;
    this->length = length;
    
    GLfloat vertex_buffer_data[] = {
        0.0f, 0.0f,-1.0f, // triangle 1 : begin
        length, length, -1.0f,
         0.0f, length, -1.0f, // triangle 1 : end
        0.0f, 0.0f,-1.0f, // triangle 2 : begin
         length, 0.0f,-1.0f,
        length, length,-1.0f, // triangle 2 : end
    };
    this->circle = create3DObject(GL_TRIANGLES, 6, vertex_buffer_data, color, GL_FILL);
}

void Circle::draw(glm::mat4 VP) {
    Matrices.model = glm::mat4(1.0f);
    glm::mat4 translate = glm::translate (this->position);    // glTranslatef
    glm::mat4 rotate    = glm::rotate((float) (this->rotation * M_PI / 180.0f), glm::vec3(1, 0, 0));
    // No need as coords centered at 0, 0, 0 of cube arouund which we waant to rotate
    // rotate          = rotate * glm::translate(glm::vec3(0, -0.6, 0));
    Matrices.model *= (translate * rotate);
    glm::mat4 MVP = VP * Matrices.model;
    glUniformMatrix4fv(Matrices.MatrixID, 1, GL_FALSE, &MVP[0][0]);
    draw3DObject(this->circle);
}

void Circle::set_position(float x, float y) {
    this->position = glm::vec3(x, y, 0);
}

int Circle::move(float flag) {
    this->position.x -= flag;
    return this->position.x;
}

int Circle::tick(float flag) {
    this->position.x += flag;
    if (this->position.y - flag < -0.5)
        return 1;
    else
        this->position.y -= flag;
    return 0;
}

int Circle::lefttick(float flag) {
    this->position.x -= flag;
    if (this->position.y - flag < -0.5)
        return 1;
    else
        this->position.y -= flag;
    return 0;
}

bounding_box_t Circle::bounding_box() {
    float x = this->position.x, y = this->position.y;
    bounding_box_t box;
    box.type = 0;
    box.x = x; box.y = y;
    box.width = length; box.height = length;
    return box;
}
