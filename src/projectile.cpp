#include "projectile.h"
#include "main.h"
#define GRAVITY 0.001

Projectile::Projectile(float x, float y, float rotation, color_t color) {
    this->position = glm::vec3(x, y, 0);
    this->rotation = rotation;
    this->acceleration = -1*GRAVITY;
    xspeed = -0.02f;
    yspeed = 0.06f;
    GLfloat vertex_buffer_data[] = {
         0.0f, 0.0f,-0.4f, // triangle 1 : begin
         0.0f, -0.5f, -0.4f,
         0.5f, -0.5f, -0.4f, // triangle 1 : end
         0.5f, -0.5f, -0.4f, // triangle 1 : end
         1.0f, -0.5f,-0.4f,
         1.0f,  0.0f,-0.4f, // triangle 2 : end
    };
    this->object = create3DObject(GL_TRIANGLES, 6, vertex_buffer_data, color, GL_FILL);
}

void Projectile::draw(glm::mat4 VP) {
    Matrices.model = glm::mat4(1.0f);
    glm::mat4 translate = glm::translate (this->position);    // glTranslatef
    glm::mat4 rotate    = glm::rotate((float) (this->rotation * M_PI / 180.0f), glm::vec3(0, 0, 1));
    // No need as coords centered at 0, 0, 0 of cube arouund which we waant to rotate
    // rotate          = rotate * glm::translate(glm::vec3(0, -0.6, 0));
    Matrices.model *= (translate * rotate);
    glm::mat4 MVP = VP * Matrices.model;
    glUniformMatrix4fv(Matrices.MatrixID, 1, GL_FALSE, &MVP[0][0]);
    draw3DObject(this->object);
}

void Projectile::set_position(float x, float y) {
    this->position = glm::vec3(x, y, 0);
}

int Projectile::tick(float flag) {
    // this->rotation += speed;
    this->position.x -= flag;
    yspeed += acceleration;
    if (this->position.y+yspeed>0)
        this->position.y += yspeed;
    else
    {
        yspeed = -1*yspeed;
        this->position.y = 0;
    }
    // this->position.y -= speed;
    return this->position.x;
}

bounding_box_t Projectile::bounding_box() {
    float x = this->position.x, y = this->position.y;
    bounding_box_t box = { x-0.3f, y+0.5f };
    box.width = 0.4; box.height = 0.4;
    return box;
}
