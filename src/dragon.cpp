#include <cmath>
#include <vector>
#include <utility>
#include "dragon.h"

using namespace std; 

Dragon::Dragon(float x, float y) {
    this->position = glm::vec3(x, y, 0);
    this->rotation = 0;
    speed = 0.01;
    
    GLfloat vertex_buffer_data[] = {
        -0.5f, -0.5f,-1.0f, // triangle 1 : begin
        -0.5f, 0.0f, -1.0f,
         0.5f, 0.0f, -1.0f, // triangle 1 : end
        0.5f, 0.0f,-1.0f, // triangle 2 : begin
         -0.5f, 0.0f,-1.0f,
         0.5f, -0.5f,-1.0f, // triangle 2 : end
        
    };

    this->body = create3DObject(GL_TRIANGLES, 6, vertex_buffer_data, COLOR_BROWN, GL_FILL);

   GLfloat vertex_buffer_data1[] = {
        -0.75f, 0.25f,-1.0f, // triangle 1 : begin
        -0.5f, -0.5f, -1.0f,
         -0.5f, 0.0f, -1.0f, // triangle 1 : end
    };
    this->wing1 = create3DObject(GL_TRIANGLES, 3, vertex_buffer_data1, COLOR_BLUE, GL_FILL);

   GLfloat vertex_buffer_data2[] = {
         0.75f, 0.25f,-1.0f, // triangle 1 : begin
         0.5f, -0.5f, -1.0f,
        0.5f, 0.0f, -1.0f, // triangle 1 : end
    };
    this->wing2 = create3DObject(GL_TRIANGLES, 3, vertex_buffer_data2, COLOR_BLUE, GL_FILL);
}

void Dragon::draw(glm::mat4 VP) {
    Matrices.model = glm::mat4(1.0f);
    glm::mat4 translate = glm::translate (this->position);    // glTranslatef
    glm::mat4 rotate    = glm::rotate((float) (this->rotation * M_PI / 180.0f), glm::vec3(1, 0, 0));
    // No need as coords centered at 0, 0, 0 of cube arouund which we waant to rotate
    // rotate          = rotate * glm::translate(glm::vec3(0, -0.6, 0));
    Matrices.model *= (translate * rotate);
    glm::mat4 MVP = VP * Matrices.model;
    glUniformMatrix4fv(Matrices.MatrixID, 1, GL_FALSE, &MVP[0][0]);
    draw3DObject(this->body);
    draw3DObject(this->wing1);
    draw3DObject(this->wing2);
}

void Dragon::set_position(float x, float y) {
    this->position = glm::vec3(x, y, 0);
}

int Dragon::move(float flag) {
    this->position.x -= flag;
    return this->position.x;
}

int Dragon::tick(float flag) {
    this->position.x += flag;
    if (this->position.y - flag < -0.5)
        return 1;
    else
        this->position.y -= flag;
    return 0;
}

bounding_box_t Dragon::bounding_box() {
    float x = this->position.x, y = this->position.y;
    bounding_box_t box;
    box.type = 0;
    box.x = x; box.y = y;
    box.width = width; box.height = height;
    return box;
}
