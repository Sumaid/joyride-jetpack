#include <iostream>
#include <cmath>
#include <vector>
#include <utility>
#include <stdlib.h> 
#include <stdio.h>
#include "main.h"
#include "timer.h"
#include "ball.h"
#include "player.h"
#include "platform.h"
#include "wall.h"
#include "firelines.h"
#include "line.h"
#include "projectile.h"
#include "seg7.h"
#include "magnet.h"
#include "boom.h"
#include "ring.h"
#include "tri.h"
#include "dragon.h"

using namespace std;

GLMatrices Matrices;
GLuint     programID;
GLFWwindow *window;

/**************************
* Customizable functions *
**************************/

Player player;
Platform platform, platform1;
vector<Wall> walls;
vector<Ball> balls;
vector<FireLine> firelines;
vector<Line> ringlines;
vector<Projectile> projectiles;
vector<Segment> segments;
vector<Circle> balloons;
vector<Circle> bullets;
vector<Magnet> magnets;
vector<Boom> booms;
vector<Ring> rings;
vector<Tri> tri;
vector<Dragon> dragons;

bool game_status;
int speed = 0.02;
int score = 0;
int level = 1;
int shielded = 0;

float screen_zoom = 1, screen_center_x = 0, screen_center_y = 0;
float camera_rotation_angle = 0;
float left_gap;
float playerx = 2.0f;
float ind = 0.0f;


int magnetflag,ringflag=0;
void draw_score(float x, float y, int score);

Timer t60(1.0 / 60);

/* Render the scene with openGL */
/* Edit this function according to your assignment */
void draw() {
    // clear the color and depth in the frame buffer
    glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // use the loaded shader program
    // Don't change unless you know what you are doing
    glUseProgram (programID);

    // Eye - Location of camera. Don't change unless you are sure!!
    glm::vec3 eye ( 0, 0, 100);
    // Target - Where is the camera looking at.  Don't change unless you are sure!!
    glm::vec3 target (0, 0, 0);
    // Up - Up vector defines tilt of camera.  Don't change unless you are sure!!
    glm::vec3 up (0, 1, 0);

    // Compute Camera matrix (view)
    Matrices.view = glm::lookAt( eye, target, up ); // Rotating Camera for 3D
    // Don't change unless you are sure!!
    // Matrices.view = glm::lookAt(glm::vec3(0, 0, 3), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0)); // Fixed camera for 2D (ortho) in XY plane

    // Compute ViewProject matrix as view/camera might not be changed for this frame (basic scenario)
    // Don't change unless you are sure!!
    glm::mat4 VP = Matrices.projection * Matrices.view;

    // Send our transformation to the currently bound shader, in the "MVP" uniform
    // For each model you render, since the MVP will be different (at least the M part)
    // Don't change unless you are sure!!
    glm::mat4 MVP;  // MVP = Projection * View * Model

    // Scene render
    //line.draw(VP);
    platform.draw(VP);
    platform1.draw(VP);
    for (int i=0; i<walls.size(); i++)
        walls[i].draw(VP);
    for (int i=0; i<balls.size(); i++)
        balls[i].draw(VP);
    //projectile.draw(VP);
    for (int i=0; i<segments.size(); i++)
        segments[i].draw(VP);
    for (int i=0; i<firelines.size(); i++)
        firelines[i].draw(VP);
    for (int i=0; i<magnets.size(); i++)
        magnets[i].draw(VP);
    for (int i=0; i<projectiles.size(); i++)
        projectiles[i].draw(VP);
    for (int i=0; i<booms.size(); i++)
        booms[i].draw(VP);
    for (int i=0; i<balloons.size(); i++)
        balloons[i].draw(VP);
    for (int i=0; i<rings.size(); i++)
        rings[i].draw(VP);
    for (int i=0; i<ringlines.size(); i++)
        ringlines[i].draw(VP);
    for (int i=0; i<tri.size(); i++)
        tri[i].draw(VP);
    for (int i=0; i<dragons.size(); i++)
        dragons[i].draw(VP);
    for (int i=0; i<bullets.size(); i++)
        bullets[i].draw(VP);
    player.draw(VP);
}

void draw_stage(float x, float y, int number)
{
    int units,seconds,hundreds;
    units=number%10;
    seconds=number/10;
    segments.push_back(Segment(x, y, 'S'));
    segments.push_back(Segment(x+0.4, y, 'T'));
    segments.push_back(Segment(x+0.8, y, 'A'));
    segments.push_back(Segment(x+1.2, y, 'G'));
    segments.push_back(Segment(x+1.6, y, 'E'));
    segments.push_back(Segment(x+2.0, y, 'q'));
    segments.push_back(Segment(x+2.4, y, seconds+'0'));
    segments.push_back(Segment(x+2.8, y, units+'0'));
}

void draw_score(float x, float y, int number)
{
    segments.clear();
    if (score==100)
    {
        level++;
        score=0;
        draw_stage(-1.5f, 5.5f, level);
    }
    else
    {
        draw_stage(-1.5f, 5.5f, level);        
    }
    int units,seconds,hundreds;
    units=number%10;
    seconds=number/10;  
    segments.push_back(Segment(x, y, 'S'));
    segments.push_back(Segment(x+0.4, y, 'C'));
    segments.push_back(Segment(x+0.8, y, '0'));
    segments.push_back(Segment(x+1.2, y, 'R'));
    segments.push_back(Segment(x+1.6, y, 'E'));
    segments.push_back(Segment(x+2.0, y, 'q'));
    segments.push_back(Segment(x+2.4, y, seconds+'0'));
    segments.push_back(Segment(x+2.8, y, units+'0'));
}


void world_creator() {
    //header
    draw_stage(-1.5f, 5.5f, level);
    draw_score(2.5f, 5.5f, score);

    //platforms
    platform = Platform(-100, -1.5, COLOR_BLACK);
    platform1 = Platform(-100, -0.5, COLOR_GREY);
    
    //walls
    for (int i=0; i<100; i++)
        walls.push_back(Wall(i*5,1,COLOR_CREAM,int(rand()%2)));

    //coins
    for (int i=0; i<6; i++)
    {
        balls.push_back(Ball(1+((double)i/2), 3, 1));
    }
    for (int i=0; i<5; i++)
    {
        balls.push_back(Ball(1+((double)i/2), 3.5, 2));
    }
    for (int i=0; i<4; i++)
    {
        balls.push_back(Ball(1.5+((double)i/2), 2.5, 1));
    }
    for (int i=0; i<7; i++)
    {
        balls.push_back(Ball(0.5+((double)i/2), 2, 2));
    }
    for (int i=0; i<7; i++)
    {
        balls.push_back(Ball(23+((double)i/2), 2, 2));
    }
    for (int i=0; i<7; i++)
    {
        balls.push_back(Ball(33+((double)i/2), 4, 2));
    }
    for (int i=0; i<8; i++)
    {
        balls.push_back(Ball(33+((double)i/2), 3, 1));
    }
    for (int i=0; i<6; i++)
    {
        balls.push_back(Ball(55+((double)i/2), 2, 1));
    }
    for (int i=0; i<4; i++)
    {
        balls.push_back(Ball(60+((double)i/2), 4, 2));
    }

    for (int i=0; i<100; i++)
    {
        for (int j=0; j<4; j++)
        {
        balls.push_back(Ball(80+5*i+((double)j/2), j+1, rand()%2));
        }        
    }

    //projectile
    projectiles.push_back(Projectile(36.0f, 1.0f, 180, COLOR_BLUE));
    projectiles.push_back(Projectile(70.0f, 1.0f, 0, COLOR_NEWGREEN));
    projectiles.push_back(Projectile(90.0f, 1.0f, 180, COLOR_BLUE));
    projectiles.push_back(Projectile(110.0f, 1.0f, 0, COLOR_NEWGREEN));
    for (int i=0; i<10; i++)
    {
        projectiles.push_back(Projectile(80+rand()%20*i, 1.0f, 180, COLOR_BLUE));
    }        

    //firelines
    firelines.push_back(FireLine(6, 3, 9, 4, 1));
    firelines.push_back(FireLine(11, 4, 14, 1, 1));
    firelines.push_back(FireLine(18, 1, 22, 1, 2));
    firelines.push_back(FireLine(44, 4, 48, 1, 1));
    firelines.push_back(FireLine(75, 4, 79, 1, 1));
    firelines.push_back(FireLine(95, 1, 99, 2, 1));
    firelines.push_back(FireLine(85, 1, 90, 1, 2));
    for (int i=0; i<100; i++)
    {
        float temp = 90+rand()%10*i;
        firelines.push_back(FireLine(temp, rand()%5+1, temp+rand()%5, rand()%5+1, 1));
    }        

    //magnet
    magnets.push_back(Magnet(30.0f, 4.4f));
    magnets.push_back(Magnet(75.0f, 4.4f));
    /*for (int i=0; i<10; i++)
    {
        magnets.push_back(Magnet(90+rand()%10*i, rand()%5+1));
    }*/        

    //boomerangs

    //rings
    rings.push_back(Ring(44.0f, 2.0f, 2.0f, COLOR_BLACK));
    rings.push_back(Ring(44.0f, 2.0f, 1.5f, COLOR_BLACK));
    rings.push_back(Ring(204.0f, 2.0f, 2.0f, COLOR_BLACK));
    rings.push_back(Ring(204.0f, 2.0f, 1.5f, COLOR_BLACK));
    rings.push_back(Ring(304.0f, 2.0f, 2.0f, COLOR_BLACK));
    rings.push_back(Ring(304.0f, 2.0f, 1.5f, COLOR_BLACK));
    
    //dragons
}

void world_mover(float speed)
{
    //tick and delete from vector
    for (int i=0; i<projectiles.size(); i++)
    {
        if (projectiles[i].tick(speed) < -5)
        {
            projectiles.erase(projectiles.begin()+i);
        }
    }
    for (int i=0; i<magnets.size(); i++)
    {
        if (magnets[i].tick(speed) < -5)
        {
            magnets.erase(magnets.begin()+i);
        }
    }
    for (int i=0; i<walls.size(); i++)
    {
        if (walls[i].tick(speed) < -5)
        {
            walls.erase(walls.begin()+i);
        }
    }
    for (int i=0; i<balls.size(); i++)
    {
        if (balls[i].tick(speed) < -5)
        {
            balls.erase(balls.begin()+i);
        }
    }
    for (int i=0; i<firelines.size(); i++)
    {
        if (firelines[i].tick(speed) < -5)
        {
            firelines.erase(firelines.begin()+i);
        }
    }
    for (int i=0; i<booms.size(); i++)
    {
        if (booms[i].tick(speed) < -5)
        {
            booms.erase(booms.begin()+i);
        }
    }
    for (int i=0; i<rings.size(); i++)
    {
        if (rings[i].tick(speed) < -5)
        {
            rings.erase(rings.begin()+i);
        }
    }
    for (int i=0; i<dragons.size(); i++)
    {
        if (dragons[i].move(speed) < -5)
        {
            dragons.erase(dragons.begin()+i);
        }
    }
}

void collision_detector(){
    //detect collisions
    for (int i=0; i<balls.size(); i++)
    {
        if (detect_collision(player.bounding_box(), balls[i].bounding_box()))
        {
            score += balls[i].type;
            draw_score(2.5f, 5.5f, score);
            balls.erase(balls.begin()+i);
        }
    }
    for (int i=0; i<projectiles.size(); i++)
    {
        if (detect_collision(player.bounding_box(), projectiles[i].bounding_box()))
        {
            //printf("Projectile Detected\n");
            if (projectiles[i].rotation==180)
                score += 10;
            else
            {
                score += 10;
            //    player.shield_change(1);
                float tempx = player.position.x;
                float tempy = player.position.y;
                shielded = 1;
                player = Player(tempx, tempy, COLOR_GREEN, 1);
            }
            draw_score(2.5f, 5.5f, score);
            projectiles.erase(projectiles.begin()+i);
        }
    }
    for (int i=0; i<firelines.size(); i++)
    {
        if ((detect_collision(player.bounding_box(), firelines[i].bounding_box()))&&(ringflag==0)&&(shielded==0))
        {
            if (score-2>0)
            {
                score=score-2;
                draw_score(2.5f, 5.5f, score);
            }
            else
            {
                score=0;
                draw_score(2.5f, 5.5f, score);
            }
            firelines.erase(firelines.begin()+i);
        }
    }
    for (int i=0; i<booms.size(); i++)
    {
        if (detect_collision(player.bounding_box(), booms[i].bounding_box())&&(ringflag==0)&&(shielded==0))
        {
            //printf("Collided\n");
            score=0;
            draw_score(2.5f, 5.5f, score);
            booms.clear();
        }
    }
    for (int i=0; i<balloons.size(); i++)
    {
        for (int j=0; j<firelines.size(); j++)
        {
            if (detect_collision(balloons[i].bounding_box(), firelines[j].bounding_box()))
            {
                //printf("Super Collided\n");
                score += 2;
                draw_score(2.5f, 5.5f, score);
                balloons.erase(balloons.begin()+i);
                firelines.erase(firelines.begin()+j);
            }
        }
    }
    for (int i=0; i<bullets.size(); i++)
    {
        if (detect_collision(player.bounding_box(), bullets[i].bounding_box())&&(shielded==0))
        {
            //printf("bullet Collided\n");
            if (score-5<0)
                score = 0;
            else
                score -= 5;
            draw_score(2.5f, 5.5f, score);
            //bullets.erase(balloons.begin()+i);
        }
    }
}
float yspeed = 0.05;
int bullet_counter = 0;
int coins_countera = 0;
int firelines_countera = 0;
int projectiles_countera = 0;
int magnet_countera = 0;
int boom_countera = 0;
int ring_countera = 0;
int dragon_countera = 0;
void auto_mover(){
    if ((playerx>30)&&(booms.size()==0)&&(playerx<32))
        booms.push_back(Boom(5.0f, 4.4f));

    if ((playerx>50)&&(dragons.size()==0)&&(playerx<52))
        dragons.push_back(Dragon(5.0f, 4.0f));
    
    if (playerx>10000000)
    {
        coins_countera++;
        firelines_countera++;
        projectiles_countera++;
        magnet_countera++;
        boom_countera++;
        ring_countera++;
        dragon_countera++;
        int selector = rand()%5;
        if (coins_countera==100)
        {
            coins_countera=0;
            for (int i=0; i<rand()%3+2; i++)
            {
                for (int j=0; j<rand()%6+2; j++)
                    balls.push_back(Ball(3+((double)i/2), rand()%4+1, rand()%2));
            }
        }
        if (firelines_countera==200)
        {
            firelines_countera = 0;
            firelines.push_back(FireLine(5, rand()%5+1, rand()%5+1, rand()%5+1, 1));
        }
        if (projectiles_countera==300)
        {
            projectiles_countera = 0;
            projectiles.push_back(Projectile(5, rand()%5+1, 0, COLOR_BLUE));
        }
        if (magnet_countera==400)
        {
            magnet_countera = 0;
            magnets.push_back(Magnet(5, rand()%5+1));
        }
        if (boom_countera==500)
        {
            boom_countera = 0;
            booms.push_back(Boom(5.0f, 4.4f));
        }
        if (ring_countera==600)
        {
            ring_countera = 0;
            rings.push_back(Ring(44.0f, 2.0f, 2.0f, COLOR_BLACK));
            rings.push_back(Ring(44.0f, 2.0f, 1.5f, COLOR_BLACK));
        }
        if (dragon_countera==700)
        {
            dragon_countera = 0;
            dragons.push_back(Dragon(5.0f, 4.0f));
        }
    }

    for (int i=0; i<firelines.size(); i++)
    {
        if (firelines[i].type==2)
        {
            //printf("TYPE 2\n");
            if ((firelines[i].position.y+yspeed<4.5)&&(firelines[i].position.y+yspeed>0))
            {   
                firelines[i].position.y+=yspeed;
                firelines[i].line.tick(0.0,yspeed);
                firelines[i].circle1.position.y+=yspeed;
                firelines[i].circle2.position.y+=yspeed;
            }
            else
            {
                yspeed *= -1;
                firelines[i].line.tick(0.0,yspeed);
                firelines[i].circle1.position.y+=yspeed;
                firelines[i].circle2.position.y+=yspeed;
            }
        }
    }
    bullet_counter++;
    for (int i=0; i<dragons.size(); i++)
    {
        if (bullet_counter>20)
        {
            bullets.push_back(Circle(dragons[i].position.x, dragons[i].position.y, 0.1, COLOR_RED));
            bullet_counter = 0;
        }
        if ((dragons[i].position.y+yspeed<4.5)&&(dragons[i].position.y+yspeed>0))
        {   
            dragons[i].position.y+=yspeed;
        }
        else
        {
            yspeed *= -1;
            dragons[i].position.y+=yspeed;
        }
    }
    for (int i=0; i<magnets.size(); i++)
    {
        int timepass = magnets[i].tick(0.01);
        float m = magnets[i].lines[1].position.x;
        float p = player.position.x-0.5;
        //printf("player x: %f\n", p);
        //printf("magnet x: %f\n", m);
        bool check1 = (p<m)&&(p+0.6>m);
        bool check2 = (p<m)&&(p+0.6>m+0.1);
        bool check3 = (m<p)&&(m+0.1>p);
        if (magnets[i].position.x < -5)
        {
            magnets.erase(magnets.begin()+i);
        }
        else if (check2||check1||check3)
        {
            //printf("Hurray\n");
            magnetflag = 1;
            if (player.position.y+0.125<4.0)
                player.position.y += 0.125;   
        }
        else
        {
            magnetflag = 0;
        }
    }

    if (rings.size()>1)
    {
        float r = rings[1].position.x-2;
        float p = player.position.x-0.5;
        float p_y = player.position.y-0.5;
        bool ringcheck1 = (p<r)&&(p+0.6>r);
        bool ringcheck2 = (p>r)&&(p<r+0.5);
        bool ringcheck3 = (p_y>2)&&(p_y<2+rings[1].radius);
        if (rings[1].position.x < -5)
        {
            rings.erase(rings.begin()+0);
            rings.erase(rings.begin()+1);
        }
        else if (((ringcheck1 || ringcheck2 ) && ringcheck3)||(ind > 0.001)&&(ind<=3.14))
        {
            player.position.y = 3.5*sin(ind)*rings[1].radius+2.0;  
            player.position.x = -3.5*cos(ind)*rings[1].radius+rings[1].position.x;
            //printf("Ringed\n");
            ringflag = 1;
        }
        else
        {
            ind = 0;
            ringflag = 0;
        }
    }    

    for (int i=0; i<booms.size(); i++)
    {
        if ((booms[i].tick(0.1)))
            booms.erase(booms.begin()+i);
    }
    for (int i=0; i<balloons.size(); i++)
    {
        if (balloons[i].tick(0.1))
            balloons.erase(balloons.begin()+i);
    }
    for (int i=0; i<bullets.size(); i++)
    {
        if (bullets[i].lefttick(0.1))
            bullets.erase(bullets.begin()+i);
    }
    for (int i=0; i<projectiles.size(); i++)
    {
        if (projectiles[i].tick(0.02) < -5)
        {
            projectiles.erase(projectiles.begin()+i);
        }
    }
}

int water_counter = 0;
float tri_counter = 0.0f;
void tick_input(GLFWwindow *window) {
    int left  = glfwGetKey(window, GLFW_KEY_LEFT);
    int right = glfwGetKey(window, GLFW_KEY_RIGHT);
    int up = glfwGetKey(window, GLFW_KEY_SPACE);
    int shift = glfwGetKey(window, GLFW_KEY_LEFT_SHIFT);
    water_counter++;
    if ((shift)&&(water_counter>5))
    {
        water_counter=0;
        //printf("Shift pressed\n");
        balloons.push_back(Circle(player.position.x+0.1, player.position.y+0.4, 0.1, COLOR_WATER));
    }

    if (ringflag)
    {
        tri.clear();
        if (right)
            ind+=0.02;
        else if (left)
            ind-=0.02;
        if (right!=0&&player.position.x>2.0)
        {
            if (level==1)
            {
                world_mover(0.1);
                playerx+=0.1;
            }
            else
            {
                world_mover(0.15);
                playerx+=0.15;
            }
        }
        return;
    }

    if (up)
    {
        tri.clear();
        if (tri_counter<0.5f)
            tri_counter += 0.01f;
        tri.push_back(Tri(player.position.x-0.2, player.position.y-0.5, tri_counter, 0, COLOR_FIRE));
    }
    else
    {
        tri.clear();
        tri_counter = 0.0f;
    }
    
    if (right!=0&&player.position.x>2.0)
    {
        if (level==1)
        {
            world_mover(0.1);
            playerx+=0.1;
        }
        else
        {
            world_mover(0.15);
            playerx+=0.15;
        }
    }
    player.move(left, right, up);
}

/* Initialize the OpenGL rendering properties */
/* Add all the models to be created here */
void initGL(GLFWwindow *window, int width, int height) {
    /* Objects should be created before any other gl function and shaders */
    // Create the models

    world_creator();
    player = Player(-1, 0, COLOR_GREEN, 0);
    //line = Line(1, 1, 1, 3, COLOR_FIRE);
    // Create and compile our GLSL program from the shaders
    programID = LoadShaders("Sample_GL.vert", "Sample_GL.frag");
    // Get a handle for our "MVP" uniform
    Matrices.MatrixID = glGetUniformLocation(programID, "MVP");


    reshapeWindow (window, width, height);

    // Background color of the scene
    glClearColor (COLOR_BACKGROUND.r / 256.0, COLOR_BACKGROUND.g / 256.0, COLOR_BACKGROUND.b / 256.0, 0.0f); // R, G, B, A
    glClearDepth (1.0f);

    glEnable (GL_DEPTH_TEST);
    glDepthFunc (GL_LEQUAL);

    cout << "VENDOR: " << glGetString(GL_VENDOR) << endl;
    cout << "RENDERER: " << glGetString(GL_RENDERER) << endl;
    cout << "VERSION: " << glGetString(GL_VERSION) << endl;
    cout << "GLSL: " << glGetString(GL_SHADING_LANGUAGE_VERSION) << endl;
}


int main(int argc, char **argv) {
    float flag = 0;
    srand(time(0));
    int width  = 800;
    int height = 800;
    

    window = initGLFW(width, height);

    initGL (window, width, height);

    while (!glfwWindowShouldClose(window)) {

    /* Draw in loop */
        if (t60.processTick()) {
            // 60 fps

            // OpenGL Draw commands
            draw();

            // Swap Frame Buffer in double buffering
            glfwSwapBuffers(window);
            reshapeWindow (window, width, height);

            //tick_elements(1);
            collision_detector();
            tick_input(window);
            auto_mover();
            //printf("Playerx: %f\n", playerx);
            // OpenGL Draw commands
            draw();

            if(game_status) break;

        }

        // Poll for Keyboard and mouse events
        glfwPollEvents();
    }

    quit(window);
}

bool detect_collision(bounding_box_t a, bounding_box_t b) {
    if (b.type==0)
        return (abs(a.x - b.x) * 2 < (a.width + b.width)) &&
                (abs(a.y - b.y) * 2 < (a.height + b.height));
    else if (b.type==1)
    {
        int lhs, rhs;
        bool answer, xrange, yrange;
        if ((b.y1)>b.y)
        {
            lhs = (b.y-b.y1)*(a.x)-(b.x-b.x1)*(a.y)+(b.x*b.y1)-(b.x1*b.y);
            rhs = (b.y-b.y1)*(a.x+a.width)-((b.x-b.x1)*(a.y+a.height))+(b.x*b.y1)-(b.x1*b.y);
            /*
            printf("b.x:%f\n b.y:%f\n b.x1:%f\n b.y1:%f\n", b.x, b.y, b.x1, b.y1);
            printf("a.x: %f, a.y: %f\n",a.x, a.y);
            printf("lhs: %d\n", lhs);
            printf("rhs: %d\n", rhs);
            */
           answer = (lhs*rhs<=0);
           xrange = ((a.x<b.x1&&a.x>b.x)||(a.x+a.width<b.x1&&a.x+a.width>b.x));
           yrange = ((a.y<b.y1&&a.y>b.y)||(a.y+a.height<b.y1&&a.y+a.height>b.y));
        }
        else
        {
            lhs = (b.y-b.y1)*(a.x+a.width)-(b.x-b.x1)*(a.y)+(b.x*b.y1)-(b.x1*b.y);
            rhs = (b.y-b.y1)*(a.x)-((b.x-b.x1)*(a.y+a.height))+(b.x*b.y1)-(b.x1*b.y);
            /*printf("b.x:%f\n b.y:%f\n b.x1:%f\n b.y1:%f\n", b.x, b.y, b.x1, b.y1);
            printf("a.x: %f, a.y: %f\n",a.x, a.y);
            printf("lhs: %d\n", lhs);
            printf("rhs: %d\n", rhs);
            */
            if (lhs==0||rhs==0)
                answer = true;
            xrange = ((a.x<b.x1&&a.x>b.x)||(a.x+a.width<b.x1&&a.x+a.width>b.x));
            yrange = ((a.y-a.height<b.y&&a.y-a.height>b.y1)||(a.y<b.y&&a.y>b.y1));
        }   
        if (answer&&xrange&&yrange)
            return true;
        else
            return false;
    }
}

void reset_screen() {
    float top    = screen_center_y + 6 / screen_zoom;
    float bottom = screen_center_y - 2 / screen_zoom;
    float left   = screen_center_x - 2 / screen_zoom;
    left_gap = left;
    float right  = screen_center_x + 6 / screen_zoom;
    Matrices.projection = glm::ortho(left, right, bottom, top, 0.1f, 500.0f);
}
