#include "main.h"
#include "circle.h"
#include "line.h"

#ifndef FIRELINES_H
#define FIRELINES_H


class FireLine {
public:
    FireLine() {}
    FireLine(float x, float y, float x1, float y1, int type);
    glm::vec3 position;
    glm::vec3 endposition;
    float rotation;
    int type;
    void draw(glm::mat4 VP);
    void set_position(float x, float y);
    int tick(float flag);
    float speed;
    float radius;
    Circle circle1;
    Circle circle2;
    Line line;
    bounding_box_t bounding_box();
private:
};

#endif // FIRELINES_H
