#include <cmath>
#include <vector>
#include <utility>
#include "firelines.h"
#include "circle.h"
#include "line.h"

using namespace std; 

FireLine::FireLine(float x, float y, float x1, float y1, int type) {
    this->type = type;
    this->position = glm::vec3(x, y, 0);
    this->endposition = glm::vec3(x1, y1, 0);
    this->speed = 0.01;
    this->radius = 0.05f;
    this->circle1 = Circle(x, y, this->radius*2, COLOR_DARKGOLD);
    this->circle2 = Circle(x1, y1, this->radius*2, COLOR_DARKGOLD);
    this->line = Line(x, y, x1, y1, 0.03f, COLOR_FIRE);
}

void FireLine::draw(glm::mat4 VP) {
    this->line.draw(VP);
    this->circle1.draw(VP);
    this->circle2.draw(VP);
}

void FireLine::set_position(float x, float y) {
    this->position = glm::vec3(x, y, 0);
}

int FireLine::tick(float flag) {
    this->position.x -= flag;
    this->endposition.x -= flag;
    this->circle2.move(flag);
    this->circle1.move(flag);
    return this->line.tick(flag, 0.0); 
}

bounding_box_t FireLine::bounding_box() {
    float x = this->position.x, y = this->position.y;
    bounding_box_t box;
    box.x = this->position.x;
    box.y = this->position.y;
    box.x1 = this->endposition.x;
    box.y1 = this->endposition.y;
    box.type = 1;
    box.width = 0.4; box.height = 0.4;
    return box;
}
