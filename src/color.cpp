#include "main.h"

const color_t COLOR_RED = { 236, 100, 75 };
const color_t COLOR_GREEN = { 135, 211, 124 };
const color_t COLOR_NEWGREEN = { 124, 252, 0 };
const color_t COLOR_BLACK = { 52, 73, 94 };
const color_t COLOR_BACKGROUND = { 242, 241, 239 };
const color_t COLOR_GREY = { 195, 195, 195 };
const color_t COLOR_CREAM = { 255, 218, 185 };
const color_t COLOR_GOLD = { 255, 215, 0 };
const color_t COLOR_AQUA = { 118, 238, 198};
const color_t COLOR_PINK = { 238, 118, 158};
const color_t COLOR_DARKGOLD = { 218, 165, 32};
const color_t COLOR_FIRE = { 226, 88, 34};
const color_t COLOR_DARKSLATEGREY = { 112, 128, 144};
const color_t COLOR_BLUE = { 0, 0, 153};
const color_t COLOR_BROWN = { 210, 105, 30};
const color_t COLOR_VIOLET = { 102, 0, 51};
const color_t COLOR_WATER = { 0, 119, 190};

