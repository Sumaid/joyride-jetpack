#include "main.h"

#ifndef DRAGON_H
#define DRAGON_H


class Dragon {
public:
    Dragon() {}
    Dragon(float x, float y);
    glm::vec3 position;
    float rotation;
    void draw(glm::mat4 VP);
    void set_position(float x, float y);
    int tick(float flag);
    int move(float flag);
    double speed;
    float width, height;
    bounding_box_t bounding_box();
private:
    VAO *body, *wing1, *wing2;
};

#endif // CIRCLE_H
