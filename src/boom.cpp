#include <cmath>
#include <vector>
#include <utility>
#include "boom.h"
#include "line.h"

using namespace std; 

Boom::Boom(float x, float y) {
    this->position = glm::vec3(x, y, 0);
    this->xspeed = 0.03;
    this->yspeed = 0.0085;
    this->rotation = 0;
    width = 0.5; height = 0.3;
    lines.push_back(Line(x, y, x, y+height, 0.03f, COLOR_VIOLET));
    lines.push_back(Line(x, y, x+width, y, 0.03f, COLOR_VIOLET));
    lines.push_back(Line(x, y+height, x+width, y+height, 0.03f, COLOR_VIOLET));
}

void Boom::draw(glm::mat4 VP) {
    for (int i=0; i<lines.size(); i++)
    {
        lines[i].draw(VP);
    }
}

void Boom::set_position(float x, float y) {
    this->position = glm::vec3(x, y, 0);
}

int Boom::tick(float flag) {
    this->position.x -= xspeed;
    this->position.y -= yspeed;
    for (int i=0; i<lines.size(); i++)
    {
        if (lines[i].position.x-xspeed>left_gap)
            lines[i].position.x -= xspeed;
        else
        {
            yspeed *= 5;
            xspeed *= -1;
        }
        if (lines[i].position.y - yspeed < -0.5)
            return 1;
        lines[i].position.y -= yspeed;
    }
    return 0;
    //this->position.x -= flag;
    //return this->position.x;
}

bounding_box_t Boom::bounding_box() {
    float x = this->position.x, y = this->position.y;
    bounding_box_t box;
    box.x = x;
    box.y = y;
    box.type = 0;
    box.width = width; box.height = height;
    return box;
}
