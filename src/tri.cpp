#include <cmath>
#include <vector>
#include <utility>
#include "tri.h"

using namespace std; 

Tri::Tri(float x, float y, float length, int rotation, color_t color) {
    this->position = glm::vec3(x, y, 0);
    this->rotation = rotation;
    speed = 0.01;
    this->length = length;
    
    GLfloat vertex_buffer_data[] = {
        0.0f, 0.0f,-1.0f, // triangle 1 : begin
        -1*length, -1*length, -1.0f,
         length, -1*length, -1.0f, // triangle 1 : end
    };
    this->tri = create3DObject(GL_TRIANGLES, 3, vertex_buffer_data, color, GL_FILL);
}

void Tri::draw(glm::mat4 VP) {
    Matrices.model = glm::mat4(1.0f);
    glm::mat4 translate = glm::translate (this->position);    // glTranslatef
    glm::mat4 rotate    = glm::rotate((float) (this->rotation * M_PI / 180.0f), glm::vec3(0, 0, 1));
    // No need as coords centered at 0, 0, 0 of cube arouund which we waant to rotate
    // rotate          = rotate * glm::translate(glm::vec3(0, -0.6, 0));
    Matrices.model *= (translate * rotate);
    glm::mat4 MVP = VP * Matrices.model;
    glUniformMatrix4fv(Matrices.MatrixID, 1, GL_FALSE, &MVP[0][0]);
    draw3DObject(this->tri);
}

void Tri::set_position(float x, float y) {
    this->position = glm::vec3(x, y, 0);
}

int Tri::tick(float flag) {
    this->position.x += flag;
    if (this->position.y - flag < -0.5)
        return 1;
    else
        this->position.y -= flag;
    return 0;
}

bounding_box_t Tri::bounding_box() {
    float x = this->position.x, y = this->position.y;
    bounding_box_t box;
    box.type = 0;
    box.x = x; box.y = y;
    box.width = length; box.height = length;
    return box;
}
