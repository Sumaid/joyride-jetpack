#include "main.h"

#ifndef SHIELD_H
#define SHIELD_H


class Shield {
public:
    Shield() {}
    Shield(float x, float y, color_t color);
    glm::vec3 position;
    float rotation;
    void draw(glm::mat4 VP);
    void set_position(float x, float y);
    int tick(float flag);
    double speed;
    float length;
    bounding_box_t bounding_box();
private:
    VAO *shield;
};

#endif // Shield_H
