#include "main.h"
#include <vector>
#include "circle.h"
#include "line.h"

using namespace std; 

#ifndef MAGNET_H
#define MAGNET_H


class Magnet {
public:
    Magnet() {}
    Magnet(float x, float y);
    glm::vec3 position;
    float rotation, height, width;
    void draw(glm::mat4 VP);
    void set_position(float x, float y);
    int tick(float flag);
    float speed;
    vector<Line> lines;
    bounding_box_t bounding_box();
private:
};

#endif // FIRELINES_H
