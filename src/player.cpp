#include "main.h"
#include <iostream>
#include "player.h"

#define GRAVITY 0.002
using namespace std;

Player::Player(float x, float y, color_t color, int flag) {
    this->position = glm::vec3(x, y, 0);
    this->rotation = 0;
    this->xspeed = 0.1;
    this->yspeed = 0.05;
    this->up_press = 0;
    this->flag = flag;
    this->down_press = 0;
    this->acceleration = GRAVITY;
    // Our vertices. Three consecutive floats give a 3D vertex; Three consecutive vertices give a triangle.
    // A cube has 6 faces with 2 triangles each, so this makes 6*2=12 triangles, and 12*3 vertices
    static const GLfloat vertex_buffer_data[] = {
        -0.5f,-0.5f,-1.0f, // triangle 1 : begin
        -0.5f, 0.5f, -1.0f,
         0.1f, 0.5f, -1.0f, // triangle 1 : end
        0.1f, 0.5f,-1.0f, // triangle 2 : begin
         0.1f,-0.5f,-1.0f,
        -0.5f, -0.5f,-1.0f, // triangle 2 : end
    };
    this->object = create3DObject(GL_TRIANGLES, 6, vertex_buffer_data, color, GL_FILL);

    static const GLfloat vertex_buffer_data1[] = {
        -0.6f,-0.6f,-1.0f, // triangle 1 : begin
        -0.6f, 0.6f, -1.0f,
         0.2f, 0.6f, -1.0f, // triangle 1 : end
        0.2f, 0.6f,-1.0f, // triangle 2 : begin
         0.2f,-0.6f,-1.0f,
        -0.6f, -0.6f,-1.0f, // triangle 2 : end
    };
    this->shield = create3DObject(GL_TRIANGLES, 6, vertex_buffer_data1, COLOR_NEWGREEN, GL_FILL);
}

void Player::draw(glm::mat4 VP) {
    Matrices.model = glm::mat4(1.0f);
    glm::mat4 translate = glm::translate (this->position);    // glTranslatef
    glm::mat4 rotate    = glm::rotate((float) (this->rotation * M_PI / 180.0f), glm::vec3(1, 0, 0));
    // No need as coords centered at 0, 0, 0 of cube arouund which we waant to rotate
    // rotate          = rotate * glm::translate(glm::vec3(0, -0.6, 0));
    Matrices.model *= (translate * rotate);
    glm::mat4 MVP = VP * Matrices.model;
    glUniformMatrix4fv(Matrices.MatrixID, 1, GL_FALSE, &MVP[0][0]);
    if (this->flag==1)
        draw3DObject(this->shield);
    draw3DObject(this->object);
}

void Player::set_position(float x, float y) {
    this->position = glm::vec3(x, y, 0);
}

void Player::tick() {
    if ((this->position.y>0)&&(magnetflag!=1))
    {
        cout << "Hurray";
        yspeed -= acceleration;
        this->position.x += xspeed;
        this->position.y += yspeed;
    }
}

void Player::move(int left, int right, int up) {
    if ((left)&&(this->position.x-xspeed>left_gap+0.5))
        this->position.x -= xspeed;
    else if ((right)&&(this->position.x+xspeed<2.1))
        this->position.x += xspeed;
    
    if ((up == GLFW_PRESS)&&(this->position.y<4.5)&&(magnetflag!=1))
    {
        if (up_press!=1)
            yspeed = 0;
        up_press = 1;
        down_press = 0;
        acceleration = GRAVITY;
        yspeed += acceleration;
        this->position.y += yspeed;
    }
    else if ((up == GLFW_RELEASE)&&(this->position.y>0)&&(down_press==0)&&(magnetflag!=1))
    {
        down_press = 1;
        yspeed = 0;
        acceleration = -1 * GRAVITY;
        yspeed += acceleration;
        //if (right)
        //    this->position.x += xspeed;
        this->position.y += yspeed;
    }
    else if (down_press==1)
    {
        up_press = 0;
        yspeed += acceleration;
        if (this->position.y+yspeed>0)
            this->position.y += yspeed;
        else
        {
            this->position.y = 0;
            down_press = 0;
            up_press = 0;
        }
    }
}

bounding_box_t Player::bounding_box() {
    float x = this->position.x, y = this->position.y;
    bounding_box_t box = { x-(float)0.5, y+(float)0.5 };
    box.type = 0;
    box.width = 0.6; box.height = 1.0;
    return box;
}
