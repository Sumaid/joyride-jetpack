#include "main.h"
#include <vector>
#include "line.h"

using namespace std; 

#ifndef BOOM_H
#define BOOM_H


class Boom {
public:
    Boom() {}
    Boom(float x, float y);
    glm::vec3 position;
    float rotation, height, width;
    void draw(glm::mat4 VP);
    void set_position(float x, float y);
    int tick(float flag);
    float xspeed, yspeed;
    vector<Line> lines;
    bounding_box_t bounding_box();
private:
};

#endif // FIRELINES_H
