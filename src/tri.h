#include "main.h"

#ifndef TRI_H
#define TRI_H


class Tri {
public:
    Tri() {}
    Tri(float x, float y, float length, int rotation, color_t color);
    glm::vec3 position;
    float rotation;
    void draw(glm::mat4 VP);
    void set_position(float x, float y);
    int tick(float flag);
    double speed;
    float length;
    bounding_box_t bounding_box();
private:
    VAO *tri;
};

#endif // Tri_H
