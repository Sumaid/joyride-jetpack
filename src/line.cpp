#include "line.h"

using namespace std; 
Line::Line(float x, float y, float x1, float y1, float thickness, color_t color) {
    this->position = glm::vec3(x, y, 0);
    this->endposition = glm::vec3(x1, y1, 0);
    this->rotation = 0;
    this->speed = 0.01;
    GLfloat vertex_buffer_data[] = {
        0.0f, 0.0f,-1.0f, // triangle 1 : begin
        thickness, thickness, -1.0f,
         x1-x, y1-y, -1.0f, // triangle 1 : end
        thickness, thickness, -1.0f,
         x1-x, y1-y, -1.0f, // triangle 1 : end
         x1-x+thickness, y1-y+thickness, -1.0f, // triangle 2 : end
    };
    this->line = create3DObject(GL_TRIANGLES, 6, vertex_buffer_data, color, GL_FILL);
}

void Line::draw(glm::mat4 VP) {
    Matrices.model = glm::mat4(1.0f);
    glm::mat4 translate = glm::translate (this->position);    // glTranslatef
    glm::mat4 rotate    = glm::rotate((float) (this->rotation * M_PI / 180.0f), glm::vec3(1, 0, 0));
    // No need as coords centered at 0, 0, 0 of cube arouund which we waant to rotate
    // rotate          = rotate * glm::translate(glm::vec3(0, -0.6, 0));
    Matrices.model *= (translate * rotate);
    glm::mat4 MVP = VP * Matrices.model;
    glUniformMatrix4fv(Matrices.MatrixID, 1, GL_FALSE, &MVP[0][0]);
    draw3DObject(this->line);
}

void Line::set_position(float x, float y) {
    this->position = glm::vec3(x, y, 0);
}

int Line::tick(float xflag=0.0, float yflag=0.0) {
    // this->rotation += speed;
    this->position.x -= xflag;
    this->position.y += yflag;
    // this->position.y -= speed;
    return this->position.x;
}

bounding_box_t Line::bounding_box() {
    float x = this->position.x, y = this->position.y;
    bounding_box_t box;
    box.x = this->position.x;
    box.y = this->position.y;
    box.x1 = this->endposition.x;
    box.y1 = this->endposition.y;
    box.type = 1;
    box.width = 0.4; box.height = 0.4;
    return box;
}
