#include "ball.h"
#include "main.h"

Ball::Ball(float x, float y, int type) {
    this->position = glm::vec3(x, y, 0);
    this->rotation = 0;
    speed = 0.01;
    if (type==1)
        this->color = COLOR_AQUA;
    else
        this->color = COLOR_GOLD;
    this->type = type;
    // Our vertices. Three consecutive floats give a 3D vertex; Three consecutive vertices give a triangle.
    // A cube has 6 faces with 2 triangles each, so this makes 6*2=12 triangles, and 12*3 vertices
    static const GLfloat vertex_buffer_data[] = {
         0.0f, 0.2f,-0.4f, // triangle 1 : begin
        -0.2f, 0.0f, -0.4f,
         0.0f, 0.0f, -0.4f, // triangle 1 : end
        0.0f, 0.0f,-0.4f, // triangle 2 : begin
         0.0f, 0.2f,-0.4f,
         0.2f,  0.0f,-0.4f, // triangle 2 : end
         0.0f, -0.2f,-0.4f, // triangle 1 : begin
        -0.2f, 0.0f, -0.4f,
         0.0f, 0.0f, -0.4f, // triangle 1 : end
        0.0f, 0.0f,-0.4f, // triangle 2 : begin
         0.0f, -0.2f,-0.04f,
         0.2f,  0.0f,-0.04f, // triangle 2 : end
         
    };
    this->object = create3DObject(GL_TRIANGLES, 12, vertex_buffer_data, this->color, GL_FILL);
}

void Ball::draw(glm::mat4 VP) {
    Matrices.model = glm::mat4(1.0f);
    glm::mat4 translate = glm::translate (this->position);    // glTranslatef
    glm::mat4 rotate    = glm::rotate((float) (this->rotation * M_PI / 180.0f), glm::vec3(1, 0, 0));
    // No need as coords centered at 0, 0, 0 of cube arouund which we waant to rotate
    // rotate          = rotate * glm::translate(glm::vec3(0, -0.6, 0));
    Matrices.model *= (translate * rotate);
    glm::mat4 MVP = VP * Matrices.model;
    glUniformMatrix4fv(Matrices.MatrixID, 1, GL_FALSE, &MVP[0][0]);
    draw3DObject(this->object);
}

void Ball::set_position(float x, float y) {
    this->position = glm::vec3(x, y, 0);
}

int Ball::tick(float flag) {
    // this->rotation += speed;
    this->position.x -= flag;
    // this->position.y -= speed;
    return this->position.x;
}

bounding_box_t Ball::bounding_box() {
    float x = this->position.x, y = this->position.y;
    bounding_box_t box = { x-0.3f, y+0.5f };
    box.type = 0;
    box.width = 0.4; box.height = 0.4;
    return box;
}
