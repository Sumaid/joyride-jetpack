#include "main.h"
#include "line.h"
#include "circle.h"

#ifndef SEG_H
#define SEG_H

using namespace std;

class Segment {
public:
    Segment() {}
    Segment(float x, float y, char input);
    glm::vec3 position;
    float rotation;
    float thick;
    void draw(glm::mat4 VP);
    void set_position(float x, float y);
    int tick(float flag);
    float speed;
    vector<Line> lines;
    vector<Circle> circles;
    bounding_box_t bounding_box();
private:
};

#endif // SEGMENT_H
