#include "main.h"

#ifndef LINE_H
#define LINE_H


class Line {
public:
    Line() {}
    Line(float x, float y, float x1, float y1, float thickness, color_t color);
    glm::vec3 position;
    glm::vec3 endposition;
    float rotation;
    float length, width;
    color_t color;
    void draw(glm::mat4 VP);
    void set_position(float x, float y);
    int tick(float xflag, float yflag);
    double speed;
    bounding_box_t bounding_box();
private:
    VAO *line;
};

#endif // LINE_H
