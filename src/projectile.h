#include "main.h"

#ifndef PROJECTILE_H
#define PROJECTILE_H


class Projectile {
public:
    Projectile() {}
    Projectile(float x, float y, float rotation, color_t color);
    glm::vec3 position;
    float rotation;
    void draw(glm::mat4 VP);
    void set_position(float x, float y);
    int tick(float flag);
    float xspeed;
    float yspeed;
    float acceleration;
    bounding_box_t bounding_box();
private:
    VAO *object;
};

#endif // BALL_H
